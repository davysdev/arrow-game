const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
const arrowKeys = [
  { key: "ArrowUp", emoji: "⬆️" },
  { key: "ArrowDown", emoji: "⬇️" },
  { key: "ArrowLeft", emoji: "⬅️" },
  { key: "ArrowRight", emoji: "➡️" }
];
const keySize = 50; // tamaño de las teclas
const timeout = 1000; // tiempo máximo para pulsar la tecla
const canvasWidth = 600; //default 400
const canvasHeight = 400;

canvas.width = canvasWidth;
canvas.height = canvasHeight;
ctx.textAlign = "center";
ctx.textBaseline = "middle";

let currentKey = null; // tecla actual que debe pulsarse
let startTime = null; // tiempo de inicio para pulsar la tecla
let timerId = null; // id del temporizador
let lastKey = null; // última tecla pulsada
let score = 0; // puntuación del jugador
let bestScore = localStorage.getItem("bestScore") || 0; // mejor puntuación del jugador
let started = false; // indica si el juego ha comenzado

const nice = new Audio("sounds/nice.mp3");
const game_over = new Audio("sounds/game_over.mp3");

function getRandomKey() {
  let randomIndex;
  do {
    randomIndex = Math.floor(Math.random() * arrowKeys.length);
  } while (arrowKeys[randomIndex].key === lastKey);
  lastKey = arrowKeys[randomIndex].key;
  return arrowKeys[randomIndex];
}

function drawKey(key) {
  ctx.clearRect(0, 0, canvasWidth, canvasHeight);
  ctx.font = "38px Arial";
  ctx.fillStyle = "#9e2ffa";
  ctx.fillText(key.emoji, canvasWidth / 2, canvasHeight / 2 + keySize / 2);
  ctx.font = "30px Arial";
  ctx.fillStyle = "#ffeeff";
  ctx.fillText(`Score: ${score}`, canvasWidth / 2, 50);
  ctx.fillText(`Best: ${bestScore}`, canvasWidth / 2, 90);
}

function startTimer() {
  timerId = setTimeout(endGame, timeout);
}

function restartTimer() {
  clearTimeout(timerId);
  startTimer();
}

function startGame() {
  if (!started) {
    started = true;
    drawKey({emoji: "[Enter] to start"});
    return;
  }
  currentKey = getRandomKey();
  drawKey(currentKey);
  startTime = new Date().getTime();
  restartTimer();
}

function endGame() {
  currentKey = null;
  started = false;
  if (score > bestScore) {
    bestScore = score;
    localStorage.setItem("bestScore", bestScore);
  }
  drawKey({emoji: "Game over [Enter] to start"});
  game_over.play();
}

window.addEventListener("keydown", function(event) {
  if (!started && event.key === "Enter") {
    started = true;
    score = 0;
    startGame();
  } else if (started) {
    const key = arrowKeys.find(k => k.key === event.key);
    if (key && key.key === currentKey.key) {
      const elapsedTime = new Date().getTime() - startTime;
      if (elapsedTime < timeout) {
        score += 1; // incrementa la puntuación en 1
        nice.play();
        restartTimer();
        startGame();
      } else {
        endGame();
      }
    } else {
      endGame();
    }
  }
});

drawKey({emoji: "[Enter] to start"});