# Arrow game

🇬🇧: This skill game challenges you to press the right arrows at the right time. The arrows appear on the screen and you must press the corresponding key as fast as possible. If you make a mistake or don't press in time, you lose.

The highest score you get will be saved in LocalStorage so you can try to beat your record.

To run the game, download the files and open the "index.html" file in your browser.

Created in HTML, CSS and JavaScript.

---

🇪🇸: Este juego de habilidad te desafía a presionar las flechas correctas en el momento adecuado. Las flechas aparecen en la pantalla y debes presionar la tecla correspondiente lo más rápido posible. Si te equivocas o no presionas a tiempo, pierdes.

La puntuación más alta que obtengas se guardará en LocalStorage para que puedas intentar superar tu récord.

Para ejecutar el juego, descarga los archivos y abre el archivo "index.html" en tu navegador.

Creado en HTML, CSS y JavaScript.